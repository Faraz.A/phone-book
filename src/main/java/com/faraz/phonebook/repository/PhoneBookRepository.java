package com.faraz.phonebook.repository;

import com.faraz.phonebook.entity.PhoneBookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface PhoneBookRepository extends JpaRepository<PhoneBookEntity,Long> {
    Optional<PhoneBookEntity> findByMobileNumber(String mobileNumber);
    boolean existsByMobileNumber(String mobileNumber);


}
