package com.faraz.phonebook.mapper;

import com.faraz.phonebook.dto.PhoneBookDto;
import com.faraz.phonebook.entity.PhoneBookEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PhoneBookMapper {
    PhoneBookEntity dtoToEntity(PhoneBookDto phoneBookDto);
    PhoneBookDto entityToDto(PhoneBookEntity phoneBookEntity);

}
