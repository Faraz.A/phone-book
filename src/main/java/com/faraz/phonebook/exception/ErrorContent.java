package com.faraz.phonebook.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorContent {
    private final String code;
    private final String message;
}
