package com.faraz.phonebook.exception;

public class UserMobileExist extends BaseException{
    private static final Long serialVersionUID=1L;

    public UserMobileExist() {
        super("EXIST","USER MOBILE EXIST");
    }
}
