package com.faraz.phonebook.exception;

public class UserNotFound extends BaseException{
    private static final Long serialVersionUID=1L;

    public UserNotFound(){
        super("NOT FOUND","USER NOT FOUND");
    }
}
