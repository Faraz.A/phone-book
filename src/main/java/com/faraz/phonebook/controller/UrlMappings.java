package com.faraz.phonebook.controller;

public class UrlMappings {
    public static final String USER="/user";
    public static final String USER_SEARCH_BY_MOBILE="/search/mobile-number";
    public static final String USER_SEARCH_BY_ID = "/search/id";
    public static final String USER_CREATE ="/create";
}
