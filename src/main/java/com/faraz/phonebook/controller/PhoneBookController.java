package com.faraz.phonebook.controller;

import com.faraz.phonebook.dto.PhoneBookDto;
import com.faraz.phonebook.service.PhoneBookService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController("/phoneBook")
@RequestMapping(UrlMappings.USER)
@AllArgsConstructor
public class PhoneBookController {
    private final PhoneBookService phoneBookService;

    @GetMapping
    public Page<PhoneBookDto> getAllContact(Pageable pageable) {
        return phoneBookService.findAll(pageable);
    }
    @GetMapping(UrlMappings.USER_SEARCH_BY_ID+"/{id}")
    public PhoneBookDto findById(@PathVariable Long id) {
        return phoneBookService.findById(id);
    }

    @GetMapping(UrlMappings.USER_SEARCH_BY_MOBILE + "/{mobileNumber}")
    public PhoneBookDto findByMobileNumber(@PathVariable String mobileNumber) {
        return phoneBookService.findByMobileNumber(mobileNumber);
    }

    @PostMapping(UrlMappings.USER_CREATE + "/{create}")
    public PhoneBookDto createContact(@RequestBody PhoneBookDto phoneBookDto) {
        return phoneBookService.createContact(phoneBookDto);
    }

    @PutMapping("/{id}")
    public PhoneBookDto updateById(@RequestBody PhoneBookDto phoneBookDto, @PathVariable Long id) {
        phoneBookDto.setId(id);
        return phoneBookService.update(phoneBookDto);
    }

    @DeleteMapping("/{id}")
    public void deleteByID(@PathVariable Long id) {
        phoneBookService.deleteById(id);
    }

}
