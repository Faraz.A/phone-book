package com.faraz.phonebook.controller;

import com.faraz.phonebook.exception.BaseException;
import com.faraz.phonebook.exception.ErrorContent;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ErrorContent> exception(BaseException exception) {
        return this.translatorException(exception.getCode(), exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ErrorContent> translatorException(String code, String message, HttpStatus status) {
        return new ResponseEntity<>(new ErrorContent(code, message), status);
    }
}
