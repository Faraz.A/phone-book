package com.faraz.phonebook.service.impl;

import com.faraz.phonebook.dto.PhoneBookDto;
import com.faraz.phonebook.entity.PhoneBookEntity;
import com.faraz.phonebook.exception.UserMobileExist;
import com.faraz.phonebook.exception.UserNotFound;
import com.faraz.phonebook.mapper.PhoneBookMapper;
import com.faraz.phonebook.repository.PhoneBookRepository;
import com.faraz.phonebook.service.PhoneBookService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@AllArgsConstructor
public class PbookServiceImpl implements PhoneBookService {
    private final PhoneBookMapper phoneBookMapper;
    private final PhoneBookRepository phoneBookRepo;

    @Override
    public Page<PhoneBookDto> findAll(Pageable pageable) {
        return phoneBookRepo.findAll(pageable)
                .map(phoneBookMapper::entityToDto);
    }

    @Override
    public PhoneBookDto findByMobileNumber(String mobileNumber) {
        Optional<PhoneBookEntity> byMobileNumber = phoneBookRepo.findByMobileNumber(mobileNumber);

        return byMobileNumber.map(phoneBookMapper::entityToDto).orElseThrow(UserNotFound::new);
    }

    @Override
    public PhoneBookDto findById(Long id) {
        Optional<PhoneBookEntity> byID = phoneBookRepo.findById(id);
        return byID.map(phoneBookMapper::entityToDto).orElseThrow(UserNotFound::new);
    }

    @Override
    public PhoneBookDto createContact(PhoneBookDto phoneBookDto) {
        PhoneBookEntity phoneBookEntity = phoneBookMapper.dtoToEntity(phoneBookDto);
        boolean isMobileNumberExist = phoneBookRepo.existsByMobileNumber(phoneBookDto.getMobileNumber());
        if (isMobileNumberExist) {
            throw new UserMobileExist();
        }
        phoneBookEntity = phoneBookRepo.save(phoneBookEntity);
        return phoneBookMapper.entityToDto(phoneBookEntity);
    }

    @Override
    public PhoneBookDto update(PhoneBookDto phoneBookDto) {
        boolean isMobileNumberExist = phoneBookRepo.existsByMobileNumber(phoneBookDto.getMobileNumber());
        if (isMobileNumberExist) {
            PhoneBookEntity phoneBookEntity = phoneBookMapper.dtoToEntity(phoneBookDto);
            phoneBookRepo.save(phoneBookEntity);
            return phoneBookMapper.entityToDto(phoneBookEntity);
        } else throw new UserNotFound();
    }

    @Override
    public void deleteById(Long id) {
        phoneBookRepo.deleteById(id);
    }
}
