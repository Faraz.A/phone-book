package com.faraz.phonebook.service;

import com.faraz.phonebook.dto.PhoneBookDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PhoneBookService {
    Page<PhoneBookDto > findAll (Pageable pageable);
    PhoneBookDto findByMobileNumber(String mobileNumber);
    PhoneBookDto findById (Long id);
    PhoneBookDto createContact(PhoneBookDto phoneBookDto);
    PhoneBookDto update(PhoneBookDto phoneBookDto);
    void deleteById(Long id);


}
