package com.faraz.phonebook.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneBookDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String email;

}
