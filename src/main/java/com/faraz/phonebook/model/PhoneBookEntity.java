package com.faraz.phonebook.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "phone_book")
@Getter
@Setter
public class PhoneBookEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String email;

}
